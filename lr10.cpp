﻿#include <iostream>
#include "windows.h"
#include <iomanip>
#include "time.h"

using namespace std;

int  main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	cout << "Массив a з випадковими елементами:" << endl;

	int a[10];
	int n = 10;
	srand(time(NULL));
	for (int i = 0; i < 10; i++)
	{
		a[i] = rand() % (n + 1) - 5;
		cout << setw(5) << a[i];
	}

	int k = 0;

	for (int i = 0; i < 10; i++)
		if (a[i] < 0)
			k++;
	cout << "\nКількість негативних елементів массиву [A]: " << k << endl;

	const int x = 10;
	int min = 0;

	for (int i = 0; i < x; i++)
		if (a[i] < a[min])
			min = i;
	cout << "Мінімальний елемент массиву a[" << min << "]: " << a[min];
}
